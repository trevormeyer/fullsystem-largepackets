#ifndef MAX7060_H
#define MAX7060_H
/*
MAX7060 Register Address Definitions 
*/
#define MAX7060_Ident_Reg_Addr 			0x00  /*Read only register*/
#define MAX7060_Conf0_Reg_Addr  		0x01
#define MAX7060_Conf1_Reg_Addr  		0x02
#define MAX7060_Conf2_Reg_Addr  		0x03
#define MAX7060_IOConf0_Reg_Addr  		0x04
#define MAX7060_IOConf1_Reg_Addr  		0x05
#define MAX7060_Tstep_Reg_Addr  		0x06
#define MAX7060_PAstep_Reg_Addr  		0x07
#define MAX7060_PApwr_Reg_Addr  		0x08
#define MAX7060_FHigh0_Reg_Addr  		0x09
#define MAX7060_FHigh1_Reg_Addr  		0x0A
#define MAX7060_FCenter0_Reg_Addr  		0x0B
#define MAX7060_FCenter1_Reg_Addr  		0x0C
#define MAX7060_Flow0_Reg_Addr  		0x0D
#define MAX7060_Flow1_Reg_Addr  		0x0E
#define MAX7060_Fload_Reg_Addr  		0x0F
#define MAX7060_Enable_Reg_Addr  	0x10
#define MAX7060_Data_Reg_Addr  		0x11
#define MAX7060_Status_Reg_Addr  		0x12  /*Read only register*/

#define MAX7060_MAXFREQ_VAL 26624
#define MAX7060_MINFREQ_VAL 18944

typedef struct _MAX7060_Params {
  uint8_t Ident; 		
  uint8_t Conf0; 		
  uint8_t Conf1; 		
  uint8_t Conf2; 		
  uint8_t IOConf0; 	
  uint8_t IOConf1; 	
  uint8_t Tstep; 		
  uint8_t PAstep; 		
  uint8_t PApwr; 		
  uint8_t FHigh0; 		
  uint8_t FHigh1; 		
  uint8_t FCenter0; 	
  uint8_t FCenter1; 	
  uint8_t FLow0; 		
  uint8_t FLow1; 		
  uint8_t FLoad_Reg; 		
  uint8_t EnableReg; 	
  uint8_t DataReg; 	
  uint8_t Status; 		
}MAX7060_Params;

#endif /* MAX7060_H */