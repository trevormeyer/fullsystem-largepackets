#ifndef CIRCULAR_BUFFER_H
#define CIRCULAR_BUFFER_H

# include <nRF.h>
# include "base_station.h"
# define SIZE        1241
# define SIZE_2D_R     8
// Initialize memory for the largest packet size possible so it never runs out of space
# define SIZE_2D_C    (LARGE_PACKET_LENGTH + 8)

typedef struct _CircularBuffer {
    int         start;  /* index of oldest element              */
    int         end;    /* index at which to write new element  */
    char     value[SIZE];  /* vector of elements                   */
}CircularBuffer;
 
void cbFlush(CircularBuffer *cb);
 
int cbIsFull(CircularBuffer *cb);

int cbIsEmpty(CircularBuffer *cb);

void cbWrite(CircularBuffer *cb, char *elem);

void cbRead(CircularBuffer *cb, char *elem);


typedef struct _CircularBuffer_2D{
    uint32_t         start;  /* index of oldest element              */
    uint32_t         end;    /* index at which to write new element  */
    uint8_t          cnt;    /* Count of number of rows filled       */
    volatile uint8_t          value[SIZE_2D_R][SIZE_2D_C];  /* vector of elements                   */
} CircularBuffer_2D;
 
void cbFlush_2D(CircularBuffer_2D *cb);
 
int cbIsFull_2D(CircularBuffer_2D *cb);

int cbIsEmpty_2D(CircularBuffer_2D *cb);

void cbWrite_2D(CircularBuffer_2D *cb, uint8_t *elem); /*Write a row of data */

void cbRead_2D(CircularBuffer_2D *cb, uint8_t *elem); /*Read a row of data*/

void cbDummyWrite_2D(CircularBuffer_2D *cb);

void cbDummyRead_2D(CircularBuffer_2D *cb);

#endif