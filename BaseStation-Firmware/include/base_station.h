#ifndef BASE_STATION_H
#define BASE_STATION_H

# include <nRF.h>


#define DEFAULT_RADIO_FREQ 8

/* SYNC(2 Bytes) + PID(2 Bytes) + TxID(1 Byte) + RxID(1 Byte) + Type(1 Byte) + Data(40 Bytes) + StimStat(5 Bytes) + Time stamp(4 Bytes) + CRC(2 Bytes) */
// Should be the length of a handshake packet
#define UART_PACKET_LEN 58
#define HANDSHAKE_PACKET_LEN 50

/*PID(2 Bytes) + TxID(1 Byte) + RxID(1 Byte) + Type(1 Byte) + Data(40 or 180 Bytes) + StimStat(5 Bytes) */
#define SMALL_PACKET_LENGTH 50
#define LARGE_PACKET_LENGTH 190

#define CRCPOLYNOMIAL 0x8408 //x^16+x^12+x^5+1, LSBit first

#define DEFAULT_HANDSHAKE_INTERVAL 100 //= 500 Every 4 seconds for Fs=5Khz



/*Base station pin definitions*/
#define LTM8025_8V_Run_Pin             4
#define LTM8025_8V_PWRGD_Pin           30
#define LT3086_Run_Pin                 0
#define LT3086_Temp_Pin                1
#define LT3086_Current_Pin             2
#define LT3086_PWRGD_Pin               3
#define LTM8025_5V_PWRGD_Pin           5

#define RED_LED_Pin                    17
#define BLUE_LED_Pin                   18
#define GREEN_LED_Pin                  19

//#define T_R_CTRL_Pin                   17 // Transmit/Receive switch control

#define SPI0_CSN_Pin                   28
#define SPI0_CLK_Pin                   26
#define SPI0_MISO_Pin                  23
#define SPI0_MOSI_Pin                  27

#define SPIS_CSN_Pin                   8
#define SPIS_CLK_Pin                   9
#define SPIS_MISO_Pin                  10
#define SPIS_MOSI_Pin                  11
#define SPIS_DATARDY_Pin               12

#define PI_GPIO_23_Pin                 13
#define PI_3V3_SENSE_Pin               29

#define MAX7060_LSHDN_Pin              21
#define MAX7060_GPO1_Pin               22
#define MAX7060_DIN_Pin                24
#define MAX7060_EN_Pin                 25

//#define USB_UART_TX_Pin  7 //UART to USB-UART bridge
//#define USB_UART_RX_Pin  6

#define PI_UART_TX_Pin  14 //UART to raspberry pi
#define PI_UART_RX_Pin  15

#define Global_Interrupts_Disable() __asm volatile("cpsid i")
#define Global_Interrupts_Enable() __asm volatile("cpsie i")


typedef struct _Base_Station_Params {
  uint8_t   New_CMD_Flag;
  uint8_t   BS_ID; //Base Station ID
  uint8_t   COMM_Freq; //Wireless communication frequency between Bionode and base station (Actual Freq= 2400Mhz+COMM_Freq)
  uint8_t   MAX7060_PAPwr; // MAX7060 final output power setting
  uint8_t   MAX7060_FCenter_H; //MAX7060 output center frequency setting, high byte
  uint8_t   MAX7060_FCenter_L; //MAX7060 output center frequency setting, low byte. See page 18 of MAX7060's datasheet
  uint8_t   AutoPwrFreq_EN; /*Enable automatic WPT power level and frequency search. When this is enabled, PAPwr, FCenter_H, and 
                             FCenter_L are ignored*/
  uint8_t   PA_EN;  //Enable/disable power supplies for PA
  uint8_t   LT3086_Current; //Current drawn by PA
  uint8_t   LT3086_Temp;   // Internal temperature of LT3086 LDO
  uint8_t   LT3086_PwrGood;//LT3086 Power-Good flag
  uint8_t   LTM8025_8V_PwrGood;//LTM8025_8V Power-Good flag
  uint32_t  nRF_Temp; //Internal temperature of nRF51822
  uint8_t   Timestamp_RST; //Time stamp reset, 1 for reset
}Base_Station_Params;

#endif

/* Data type definitions */
#define Data_8bit_Small     0
#define Data_10bit_Small    1
#define Data_8bit_Large     9
#define Data_10bit_Large   10
#define BS_CONFIG           5
#define ID_PING             7

/* TX/RX ID definitions */
#define ID_BS       1
#define ID_BIONODE  2

/* UART Packet structure definition*/
#define PID_OFFSET                    2
#define TXID_OFFSET                   4
#define RXID_OFFSET                   5
#define DATA_TYPE_OFFSET              6
#define POS_ADC_TIMER1_PRESCALE_REG  23
#define POS_ADC_TIMER1_CCO_REG       24
#define RADIO_TX_TIMER1_PRESCALE_REG 21
#define RADIO_TX_TIMER1_CCO_REG      22

/* BS_CONFIG UART payload definition*/

#define BS_ID                   7   //RO
#define POS_COMM_FREQ           8   //WR
#define POS_MAX7060_PAPWR       9   //WR
#define POS_MAX7060_FCENTER_H   10  //WR
#define POS_MAX7060_FCENTER_L   11  //WR  
#define POS_AUTO_FREQPWR_EN     12  //WR
#define POS_PA_EN               13  //WR
#define POS_LT3086_CURRENT      14  //RO
#define POS_LT3086_TEMP         15  //RO
#define POS_NRF_TEMP_3          16  //RO
#define POS_NRF_TEMP_2          17  //RO
#define POS_NRF_TEMP_1          18  //RO
#define POS_NRF_TEMP_0          19  //RO
#define POS_TS_RST              50  //WO
#define POS_BS_ERR_MSG          51  //RO

