# include <nRF.h>
# include <string.h>
#include "./include/circular_buffer.h"
#include "./include/base_station.h" 
#include "./include/max7060.h"
#define Global_Interrupts_Disable() __asm volatile("cpsid i")
#define Global_Interrupts_Enable() __asm volatile("cpsie i")
  uint16_t Temp_Val;
  uint16_t Temp_Freq  = 0; //TODO: Remove me, for test only

int ADC_Init(void);
int Radio_Init(uint8_t Freq);
int GPIO_Init(void);
int GPIOTE_Init(void);
int UART_Init(void);
int UART_Write(uint8_t);
int PPI_Init(void);
int Timer0_Init(void);
int Timer1_Init(void);
int Timer2_Init(void);
int SPIS_Init(void);
int UART_Write_String(uint8_t *Data, uint8_t Len);
int Assemble_Packet(CircularBuffer_2D*);
uint16_t CRC_Gen (uint8_t *Data, uint8_t Len, uint16_t CRCPoly);
int SPI_Init();
uint8_t SPI_TransferByte(uint8_t);
void CSN_Low(void);
void CSN_High(void);
uint8_t MAX7060_Config(uint8_t Initial_Reg_Addr, uint8_t *data, uint8_t Len);
uint8_t MAX7060_Reset(void);
uint16_t PacketLoss=0;


//PID(2 Bytes) + TxID(1 Byte) + RxID(1 Byte) + Type(1 Byte) + Data(40 Bytes) + StimStat(5 Bytes) + Time Stamp(4 Bytes)

static uint8_t LedRed_Flag=0;
static uint8_t LedGreen_Flag=0;
static uint8_t LedBlue_Flag=0;


const uint8_t Mask1[4]={0xff, 0x3f, 0x0f, 0x3};
const uint8_t Mask2[4]={0xc0, 0xf0, 0xfc, 0xff};	
CircularBuffer_2D SPIS_TX_FIFO; 


//This will be set to zero before the device start TX, so that this handler can distinguish between Tx and Rx disable event
//The radio interrupt does not tell if the Disabled event is from Tx or Rx, so this is necessary
_Bool isRxEvent = 1;

// Initial Default Radio Packet Length for the radio and the SPI line
int RADIO_PACKET_LEN = SMALL_PACKET_LENGTH;
/* <0x80> + FIFO_Cnt (1 Byte) + RADIO_PACKET_LEN + Time stamp(4 Bytes) + CRC(2 Bytes) */
int SPIS_PACKET_LEN = SMALL_PACKET_LENGTH + 8;

// Initialize memory for the largest packet size possible so it never runs out of space
volatile  uint8_t SPIS_RXData[LARGE_PACKET_LENGTH];
uint8_t RadioTxPacket[HANDSHAKE_PACKET_LEN];
uint8_t RadioTxPacket_Empty[HANDSHAKE_PACKET_LEN]; 
volatile  uint8_t UART_DataIn[UART_PACKET_LEN];
_Bool changePktLen = 0;
_Bool altTransmitListen = 0;
uint8_t prevPrescale = 0x00;
uint8_t prevCompare = 0x00;
uint32_t samplesWaited = 0;
_Bool lostConnection = 1;

uint8_t isRadioTxPacketReady = 1 ;
uint16_t Handshake_Interval = DEFAULT_HANDSHAKE_INTERVAL;
_Bool New_Bionode_CMD_Flag = 0;
Base_Station_Params BS_Params_Old, BS_Params_New;
MAX7060_Params MAX7060_Reg;

void main (void){


  //Product Anomaly. The following two lines of code must be executed to use peripherals
  //Please refer to nRF51822-PAN v2.0 Page 25 entry #26: System: Manual setup is required to enable use of peripherals.
  *(uint32_t *)0x40000504 = 0xC007FFDF;
  *(uint32_t *)0x40006C18 = 0x00008000;
  //Start Crystal Oscillator 
  NRF_CLOCK->TASKS_HFCLKSTART = 0X01;
  while(NRF_CLOCK->EVENTS_HFCLKSTARTED == 0){
  }
  for(int i=0;i<sizeof(RadioTxPacket_Empty);i++){
    RadioTxPacket_Empty[i]=0;
  }
  GPIO_Init();
  //GPIOTE_Init();
  UART_Init();
  SPI_Init();
  MAX7060_Reset();
  //RFGen_Init(DEFAULT_AD9834_FREQ_REQ, 0); //8.01MHz
  NRF_GPIO->OUTCLR = (1<<RED_LED_Pin);

  for (int i=0;i<1000000;i++){}
  NRF_GPIO->OUTSET = (1<<RED_LED_Pin);

  NRF_GPIO->OUTCLR = (1<<GREEN_LED_Pin);
  for (int i=0;i<1000000;i++){}
  NRF_GPIO->OUTSET = (1<<GREEN_LED_Pin);

  NRF_GPIO->OUTCLR = (1<<BLUE_LED_Pin);
  for (int i=0;i<1000000;i++){}
  NRF_GPIO->OUTSET = (1<<BLUE_LED_Pin);

  memset (&MAX7060_Reg, 0, sizeof(MAX7060_Reg));

  
  cbFlush_2D((CircularBuffer_2D *) &SPIS_TX_FIFO);
  //TODO: Initialize all elements in column one of the buffer to 0x80
  for (int i=0; i<SIZE_2D_R; i++){
   SPIS_TX_FIFO.value[i][0]=0x80;
  }

  PPI_Init();
  Radio_Init(DEFAULT_RADIO_FREQ);
  Timer0_Init();
  Timer1_Init();
  Timer2_Init();
  SPIS_Init();
  ADC_Init();
  NVIC_SetPriority(UART0_IRQn,1);
  NVIC_SetPriority(RADIO_IRQn,2);
  NVIC_SetPriority(TIMER1_IRQn,3);
  NVIC_SetPriority(TIMER2_IRQn,3);
  NVIC_SetPriority(ADC_IRQn,4);
  NVIC_SetPriority(SPI1_TWI1_IRQn,4);
  NVIC_EnableIRQ(TIMER1_IRQn);
  NVIC_EnableIRQ(TIMER2_IRQn);
  NVIC_EnableIRQ(RADIO_IRQn);
  NVIC_EnableIRQ(UART0_IRQn);
  NVIC_EnableIRQ(SPI1_TWI1_IRQn);
  NVIC_EnableIRQ(ADC_IRQn);
  while(1){
    int i;
    if (!cbIsEmpty_2D(&SPIS_TX_FIFO)){
       //TODO: Assembly packet for SPIS
     
    }
    if (cbIsFull_2D(&SPIS_TX_FIFO)){
      //TODO: Turn on Led2
//      NVIC_DisableIRQ(TIMER1_IRQn);
//      NVIC_DisableIRQ(RADIO_IRQn);
//      NVIC_DisableIRQ(UART0_IRQn);
//      while(1){}
    }
    else{
      //TODO: Turn off Led2
      i++;
    }

    if (BS_Params_New.New_CMD_Flag){
      NVIC_DisableIRQ(UART0_IRQn);
      BS_Params_New.New_CMD_Flag = 0;
      if (BS_Params_New.Timestamp_RST){
        NRF_TIMER0->TASKS_CLEAR = 1;
      }
      if(NRF_RADIO->FREQUENCY != BS_Params_New.COMM_Freq){
        NVIC_DisableIRQ(RADIO_IRQn);
        NRF_RADIO->TASKS_DISABLE=1;
        while(NRF_RADIO->STATE !=0){}
        NVIC_ClearPendingIRQ(RADIO_IRQn);
        NRF_RADIO->FREQUENCY = BS_Params_New.COMM_Freq;
        NRF_RADIO->TASKS_RXEN = 1;
        //NRF_GPIO->OUTCLR = 1<< T_R_CTRL_Pin;

        NVIC_EnableIRQ(RADIO_IRQn);
      }

      if ((BS_Params_New.MAX7060_FCenter_H != BS_Params_Old.MAX7060_FCenter_H) ||
          (BS_Params_New.MAX7060_FCenter_L != BS_Params_Old.MAX7060_FCenter_L) ||
          (BS_Params_New.MAX7060_PAPwr!=BS_Params_Old.MAX7060_PAPwr)||
          BS_Params_New.AutoPwrFreq_EN!=BS_Params_Old.AutoPwrFreq_EN)  {
        //MAX7060_Reset();
        //memset (&MAX7060_Reg.FCenter0, 0,  (MAX7060_Flow1_Reg_Addr - MAX7060_FHigh0_Reg_Addr + 1));
        BS_Params_Old.MAX7060_FCenter_H = BS_Params_New.MAX7060_FCenter_H; 
        BS_Params_Old.MAX7060_FCenter_L = BS_Params_New.MAX7060_FCenter_L;
        BS_Params_Old.MAX7060_PAPwr = BS_Params_New.MAX7060_PAPwr;
        BS_Params_Old.AutoPwrFreq_EN = BS_Params_New.AutoPwrFreq_EN;
        //TODO: reconfig max7060
        MAX7060_Reg.FCenter0 = BS_Params_Old.MAX7060_FCenter_H;
        MAX7060_Reg.FCenter1 = BS_Params_Old.MAX7060_FCenter_L;
        MAX7060_Reg.FHigh0 = BS_Params_Old.MAX7060_FCenter_H;
        MAX7060_Reg.FHigh1 = BS_Params_Old.MAX7060_FCenter_L;
        MAX7060_Reg.FLow0 = BS_Params_Old.MAX7060_FCenter_H;
        MAX7060_Reg.FLow1 = BS_Params_Old.MAX7060_FCenter_L;
        MAX7060_Reg.PApwr = BS_Params_Old.MAX7060_PAPwr;
        MAX7060_Reg.FLoad_Reg = 1;
        MAX7060_Config(MAX7060_Conf0_Reg_Addr, &MAX7060_Reg.Conf0, (MAX7060_Data_Reg_Addr - MAX7060_Conf0_Reg_Addr + 1));//There are two read only registers


      }
      if ((BS_Params_New.PA_EN == 1) && (BS_Params_Old.PA_EN == 0)){
        BS_Params_Old.PA_EN = BS_Params_New.PA_EN;
        MAX7060_Reg.EnableReg =  BS_Params_Old.PA_EN;
        MAX7060_Reg.DataReg = BS_Params_Old.PA_EN;
        BS_Params_Old.MAX7060_FCenter_H = BS_Params_New.MAX7060_FCenter_H; 
        BS_Params_Old.MAX7060_FCenter_L = BS_Params_New.MAX7060_FCenter_L;
        BS_Params_Old.MAX7060_PAPwr = BS_Params_New.MAX7060_PAPwr;
        //TODO: Enable MAX7060 output
        NRF_GPIO->OUTSET = 1<<LTM8025_8V_Run_Pin; 
        NRF_GPIO->OUTSET = 1<<LT3086_Run_Pin;
         //MAX7060_Reset();
        //memset (&MAX7060_Reg.FCenter0, 0,  (MAX7060_Flow1_Reg_Addr - MAX7060_FHigh0_Reg_Addr + 1));
        //TODO: reconfig max7060
        MAX7060_Reg.FCenter0 = BS_Params_Old.MAX7060_FCenter_H;
        MAX7060_Reg.FCenter1 = BS_Params_Old.MAX7060_FCenter_L;
        MAX7060_Reg.FHigh0 = BS_Params_Old.MAX7060_FCenter_H;
        MAX7060_Reg.FHigh1 = BS_Params_Old.MAX7060_FCenter_L;
        MAX7060_Reg.FLow0 = BS_Params_Old.MAX7060_FCenter_H;
        MAX7060_Reg.FLow1 = BS_Params_Old.MAX7060_FCenter_L;
        MAX7060_Reg.PApwr = BS_Params_Old.MAX7060_PAPwr;
        MAX7060_Reg.FLoad_Reg = 1;
        MAX7060_Config(MAX7060_Conf0_Reg_Addr, &MAX7060_Reg.Conf0, (MAX7060_Data_Reg_Addr - MAX7060_Conf0_Reg_Addr + 1));//There are two read only registers
 
      }
      else if((BS_Params_New.PA_EN ==0) && (BS_Params_Old.PA_EN == 1)){
        BS_Params_Old.PA_EN = BS_Params_New.PA_EN;
        //TODO: Disable MAX7060 output
        //TODO: Turn off LTM8025_8V, and LT3086 7V LDO
        MAX7060_Reset();
        memset (&MAX7060_Reg, 0, sizeof(MAX7060_Reg));
        NRF_GPIO->OUTCLR = 1<<LT3086_Run_Pin;
        NRF_GPIO->OUTCLR = 1<<LTM8025_8V_Run_Pin; 
      }
      NVIC_EnableIRQ(UART0_IRQn);
      //TODO: Grab read only base station parameters and construct a reply packet
    }//end of if (BS_Params_New.New_CMD_Flag)

    if (New_Bionode_CMD_Flag){
      New_Bionode_CMD_Flag=0;
      NVIC_DisableIRQ(UART0_IRQn);
      isRadioTxPacketReady = 0;
      //if ((UART_DataIn[0] == 0xA5) && (UART_DataIn[1] == 0x5A))
      //{
      for(int i=0;i<sizeof(RadioTxPacket);i++){
        RadioTxPacket[i]=UART_DataIn[i+2];
      }
      isRadioTxPacketReady = 1;
      //}
      NVIC_EnableIRQ(UART0_IRQn);
    }
  }
}

int ADC_Init(void){
  NRF_ADC->ENABLE=0x01; //Enable ADC
  //NRF_ADC->CONFIG = 0x0806; //10bit resolution, 2/3 input prescaling, 1.2V Bandgap ref, USE AIN3
  //NRF_ADC->CONFIG = 0x0406; //10bit resolution, 2/3 input prescaling, 1.2V Bandgap ref, USE AIN2
  //NRF_ADC->CONFIG = 0x0422; //10bit resolution, NO input prescaling, 1.2V Bandgap ref, USE AIN2
  //NRF_ADC->CONFIG = 0x10422; //10bit resolution, NO input prescaling, AREF0 Ext. ref, USE AIN2
  //NRF_ADC->CONFIG = 0x1002; //10bit resolution, NO input prescaling, 1.2V Bandgap ref, USE AIN4
  //NRF_ADC->CONFIG = 0x11022; //10bit resolution, NO input prescaling, AREF0 Ext. ref, USE AIN4
  //NRF_ADC->CONFIG = ADC_RESOLUTION_10BIT | ADC_INPPSEL_AIN_NO_PS | ADC_REFSEL_VBG|ADC_PSEL_AIN2
  NRF_ADC->CONFIG =0x800A; //8bit resolution, NO input prescaling, 1.2V Bandgap ref, USE AIN2
  NRF_ADC->INTENSET = 0x01; //Enable interrupt for ADC END event
  NRF_GPIO->DIRCLR = 1<<6; //Ensure P0.02 is set as input
  return 0;
}

int Radio_Init(uint8_t Freq){
  NRF_RADIO->POWER |= 0x01;// Power on radio peripheral
  NRF_RADIO->SHORTS |= 0X03; // Enable shortcuts between Ready/Start, End/Disable
  NRF_RADIO->INTENSET = 0x08;
  NRF_RADIO->PACKETPTR = &(SPIS_TX_FIFO.value[SPIS_TX_FIFO.end][2]); //Payload starting address, first two elements are reserved for SPIS Comm.
  NRF_RADIO->TXPOWER = 0x04; //Set Tx power to +4 dBm
  NRF_RADIO->MODE=0X1; // Set on-air data rate to 2Mbps
  NRF_RADIO->PCNF0 = 0x08; // Set the length of S0, Length, S1 fields to 0
  
  //Set the Maximum length of packet payload to be 170 bytes; Set base address length to 2 big Endian
  //NRF_RADIO->PCNF1 = 0x104AAAA;
  //Reserve the largest packet length necessary
  NRF_RADIO->PCNF1 = (LARGE_PACKET_LENGTH << RADIO_PCNF1_MAXLEN_Pos) | 
                     (LARGE_PACKET_LENGTH << RADIO_PCNF1_STATLEN_Pos) | 
                     (0x04 << RADIO_PCNF1_BALEN_Pos) | 
                     (RADIO_PCNF1_ENDIAN_Big << RADIO_PCNF1_ENDIAN_Pos) | 
                     (RADIO_PCNF1_WHITEEN_Disabled << RADIO_PCNF1_WHITEEN_Pos);

 //Set the address to 1,1,4
  NRF_RADIO->BASE0 = 0x80201234; 
  NRF_RADIO->PREFIX0 = 0x80;
  NRF_RADIO->TXADDRESS = 0x00;
  NRF_RADIO->RXADDRESSES = 0x01;//Enable receive channel 0
  NRF_RADIO->CRCCNF = 0x02; //One byte CRC
  NRF_RADIO->CRCINIT = 0xFFFF;
  //NRF_RADIO->CRCPOLY = 0x107; // CRC polynomial = x^8 + x^2 + x + 1;
  NRF_RADIO->CRCPOLY = 0x11021;
  NRF_RADIO->FREQUENCY =Freq;

  // Change radio length to the inital packet length
  NRF_RADIO->PCNF1 = (NRF_RADIO->PCNF1 & ~(0xFFFF)) | 
                     (RADIO_PACKET_LEN << RADIO_PCNF1_MAXLEN_Pos) | 
                     (RADIO_PACKET_LEN << RADIO_PCNF1_STATLEN_Pos);
  NRF_RADIO->TASKS_RXEN = 0x01;
  return 0;
}


int GPIO_Init(void){
  //Led configuration
  NRF_GPIO->OUTSET= (1<<RED_LED_Pin) | (1<<BLUE_LED_Pin) | (1<<GREEN_LED_Pin); 
  NRF_GPIO->DIRSET= (1<<RED_LED_Pin) | (1<<BLUE_LED_Pin) | (1<<GREEN_LED_Pin); 

  //Power supply and MAX7060 control pin configuration 
  NRF_GPIO->DIRSET= (1<<LT3086_Run_Pin) | (1<<LTM8025_8V_Run_Pin)|(1<<MAX7060_LSHDN_Pin); 
  NRF_GPIO->PIN_CNF[LTM8025_5V_PWRGD_Pin] = 0;
  NRF_GPIO->PIN_CNF[LTM8025_8V_PWRGD_Pin] = 0;
  NRF_GPIO->PIN_CNF[LT3086_PWRGD_Pin] = 0;
  NRF_GPIO->OUTCLR = 1<<MAX7060_LSHDN_Pin;

  //This pin is for sensing whether an RPi is attached
  NRF_GPIO->PIN_CNF[PI_3V3_SENSE_Pin] = 0;


  return 0;
}
//int GPIO_Init(void){
//  NRF_GPIO->OUTSET=1<<AD9834_RST_PIN; // Keep AD9834 in reset
//  NRF_GPIO->DIRSET = (1<<4) | (1<<5) | (1<<6) | (1<<7);
//  // Setting GPIO pin 10, 11 ,12 as output, prepare pin 30 as UART TX
//  NRF_GPIO->DIRSET =1<<18;//TODO: Remove me. For test only
//  NRF_GPIO->DIRSET = (1<<11)|(1<<12)|(1<<13)|(1<<14) | (1<<25) | (1<<28) | (1<<29) ; // Setting GPIO pin 5, 10, 11 ,12 as output
//  NRF_GPIO->OUTCLR = 1<<11;
//  NRF_GPIO->OUTCLR=1<<AD9834_SLP_PIN;//This pin is not really used. Make sure it is set to zero always
//  for(int i=0;i<1000;i++){} 
//  NRF_GPIO->OUTSET = (1<<25) | (1<<28) | (1<<29);
//
//  return 0;
//}

int GPIOTE_Init(void){
//  NRF_GPIOTE->CONFIG[0]= (3<<0) | (8<<8) | (3<<16) | (0<<20); //USE p0.08, low output initially, toggle
//  NRF_GPIOTE->CONFIG[1]= (3<<0) | (9<<8) | (3<<16) | (1<<20); //USE p0.09, high output initially, toggle
  return 0;
}

int UART_Init(void){

  //NRF_UART0->PSELTXD = 30; //Select GPIO pin 30 for TX; MY boards
  //NRF_UART0->PSELTXD = 9;//Eval Board UART port
  //NRF_UART0->PSELTXD = PI_UART_TX_Pin;
  NRF_UART0->PSELRXD = PI_UART_RX_Pin;
  NRF_UART0->CONFIG = 0x00;
  NRF_UART0->INTENSET = 0x04;
  NRF_UART0->BAUDRATE = 0x01D7E000; // 115200Baud
  //NRF_UART0->BAUDRATE = 0x03AFB000; //230400Baud
  //NRF_UART0->BAUDRATE = 0x075F7000; //460800Baud
  //NRF_UART0->BAUDRATE = 0x0EBEDFA4; //921600Baud
  NRF_UART0->ENABLE = 0x04; // Enable UART
  NRF_UART0->TASKS_STARTTX = 1;
  NRF_UART0->TASKS_STARTRX = 1;
  NRF_UART0->TXD = 0x00;// Send 1 byte to trigger TXRDY event
  return 0;
}

int UART_Write(uint8_t Data){
  while(NRF_UART0->EVENTS_TXDRDY == 0){}
  NRF_UART0->EVENTS_TXDRDY = 0;
  NRF_UART0->TXD = Data;
  
  return 0;
}

int UART_Write_String(uint8_t *Data, uint8_t Len){
  for (int i=0; i<Len; i++){
    UART_Write(*Data);
    Data++;
  }
  return 0;
}

int Timer2_Init(void){
  //NRF_TIMER2->SHORTS = 0x01; // Enable shortcut between COMPARE0 and CLEAR task
  //NRF_TIMER2->MODE = 0x00; //Select timer mode
  //NRF_TIMER2->PRESCALER = 0x07; //Timer Clock = 16Mhz/2^7 = 125Khz.Set COMPARE0 to 25 to achieve 5khz sampling frequency for ADC
  //NRF_TIMER2->BITMODE = 0x00; //16bit timer
  //NRF_TIMER2->CC[0] = 25000 ;//Generate interrupt every 200ms
  // //NRF_TIMER2->INTENSET = 1<<16; //Enable COMPARE0 event interrupt. Leave this commented
  //NRF_TIMER2->TASKS_START = 1;

  NRF_TIMER2->SHORTS = 0x01; // Enable shortcut between COMPARE0 and CLEAR task
  NRF_TIMER2->MODE = 0x00; //Select timer mode
  NRF_TIMER2->PRESCALER = 0x07; //Timer Clock = 16Mhz/2^7 = 125Khz.Set COMPARE0 to 25 to achieve 5khz sampling frequency for ADC
  NRF_TIMER2->BITMODE = 0x00; //16bit timer
  NRF_TIMER2->CC[0] = 200;
  NRF_TIMER2->INTENSET = 1<<16; //Enable COMPARE0 event interrupt.
  NRF_TIMER2->TASKS_START = 1;
  return 0;
}

int Timer1_Init(void){
  NRF_TIMER1->SHORTS = 0x01; // Enable shortcut between COMPARE0 and CLEAR task
  NRF_TIMER1->MODE = 0x00; //Select timer mode
  NRF_TIMER1->PRESCALER = 0x07; //Timer Clock = 16Mhz/2^7 = 125Khz.Set COMPARE0 to 25 to achieve 5khz sampling frequency for ADC
  NRF_TIMER1->BITMODE = 0x01; //8bit timer
  //NRF_TIMER0->CC[0] = 25; //Use 125Khz clock to derive 5khz sampling frequency for ADC
  NRF_TIMER1->CC[0] = 125;//Generate interrupt every 10ms
  NRF_TIMER1->INTENSET = 1<<16; //Enable COMPARE0 event interrupt.
  NRF_TIMER1->TASKS_START = 1;
  return 0;
}

int Timer0_Init(void){
  NRF_TIMER0->MODE = 0x00; //Select timer mode
  NRF_TIMER0->PRESCALER = 0x09; //Timer Clock = 16Mhz/2^9 = 31.25Khz.
  NRF_TIMER0->BITMODE = 0x03; //32bit timer
  NRF_TIMER0->TASKS_START = 1;
  return 0;
}

int PPI_Init(void){
  NRF_PPI->CH[0].EEP = &NRF_RADIO->EVENTS_DISABLED;
  NRF_PPI->CH[0].TEP = &NRF_TIMER0->TASKS_CAPTURE[0];
  NRF_PPI->CHENSET = 1<<0; //Enable PPI Ch0
  return 0;
}

int SPI_Init(){
  NRF_GPIO->DIRSET = (1<<SPI0_CSN_Pin) | (1<<SPI0_CLK_Pin) | (1 << SPI0_MOSI_Pin);
  NRF_GPIO->DIRCLR = (1<<SPI0_MISO_Pin) ;
  NRF_SPI0->ENABLE = 1;
  NRF_SPI0->PSELSCK = SPI0_CLK_Pin;
  NRF_SPI0->PSELMOSI = SPI0_MOSI_Pin;
  NRF_SPI0->PSELMISO = SPI0_MISO_Pin;
  NRF_SPI0->CONFIG = 0x00;
  NRF_SPI0->FREQUENCY = 0x10000000;
  return 0;
}

int SPIS_Init(void){
  NRF_GPIO->OUTSET = 0<<SPIS_DATARDY_Pin;  // 1 Indicates data ready
  NRF_GPIO->DIRSET = 1<<SPIS_DATARDY_Pin;

  NRF_SPIS1->INTENSET = 2;
  NRF_SPIS1->SHORTS = (1<<2);
  NRF_SPIS1->RXDPTR = &SPIS_RXData;
  NRF_SPIS1->TXDPTR = &(SPIS_TX_FIFO.value[SPIS_TX_FIFO.start][0]);
  NRF_SPIS1->PSELCSN = SPIS_CSN_Pin;
  NRF_SPIS1->PSELSCK=SPIS_CLK_Pin;
  NRF_SPIS1->PSELMOSI=SPIS_MOSI_Pin;
  NRF_SPIS1->PSELMISO=SPIS_MISO_Pin;
  NRF_SPIS1->DEF = 0x00;
  NRF_SPIS1->ORC = 0xFE;
  NRF_SPIS1->MAXRX = LARGE_PACKET_LENGTH;
  NRF_SPIS1->MAXTX = SPIS_PACKET_LEN;
  NRF_SPIS1->ENABLE = 2;
  NRF_SPIS1->TASKS_RELEASE=1;
  return 0;
}


void UART0_IRQHandler (void){
  static uint8_t cnt = 0;
  static uint8_t temp = 0;
  NVIC_DisableIRQ(UART0_IRQn);
  NRF_UART0->EVENTS_RXDRDY = 0;
  UART_DataIn[cnt] = NRF_UART0->RXD;
  cnt++;

  if (cnt==1){
    if (UART_DataIn[0]!=0xA5){
      cnt=0;
    }
  }

  if (cnt==2){
    if (UART_DataIn[1]!=0x5A){
      cnt=0;
    }
  }
  if (cnt==(UART_PACKET_LEN)){
     cnt=0;
    if (!CRC_Gen(&UART_DataIn[PID_OFFSET],UART_PACKET_LEN-2,CRCPOLYNOMIAL)){ //Calc the CRC of the packet, excluding Sync bytes
      if (UART_DataIn[RXID_OFFSET] == ID_BS){ // If msg is intended for base station
        if (UART_DataIn[DATA_TYPE_OFFSET]==BS_CONFIG){//msg is of type "base station config"
          BS_Params_New.COMM_Freq = UART_DataIn[POS_COMM_FREQ];
          BS_Params_New.MAX7060_PAPwr=UART_DataIn[POS_MAX7060_PAPWR ];
          BS_Params_New.MAX7060_FCenter_H =UART_DataIn[POS_MAX7060_FCENTER_H];
          BS_Params_New.MAX7060_FCenter_L = UART_DataIn[POS_MAX7060_FCENTER_L];
          BS_Params_New.AutoPwrFreq_EN =UART_DataIn[POS_AUTO_FREQPWR_EN];
          BS_Params_New.PA_EN = UART_DataIn[POS_PA_EN];
          BS_Params_New.Timestamp_RST = UART_DataIn[POS_TS_RST];
          BS_Params_New.New_CMD_Flag = 1;
        }
        else if (UART_DataIn[DATA_TYPE_OFFSET]==ID_PING) {
        //TODO: Reply PC with current base station parameters
        }
        else{//TODO: Indicate ERROR, This should never happen
          __NOP();
        }
      }
      else if(UART_DataIn[RXID_OFFSET] == ID_BIONODE){
        if (((UART_DataIn[POS_ADC_TIMER1_PRESCALE_REG] == 0x02) && (RADIO_PACKET_LEN != LARGE_PACKET_LENGTH)) ||
            ((UART_DataIn[POS_ADC_TIMER1_PRESCALE_REG] == 0x07) && (RADIO_PACKET_LEN != SMALL_PACKET_LENGTH))) {
              changePktLen = 1;
              
              prevPrescale = (RadioTxPacket[RADIO_TX_TIMER1_PRESCALE_REG] ? RadioTxPacket[RADIO_TX_TIMER1_PRESCALE_REG] : 0x07);
              prevCompare  = (RadioTxPacket[RADIO_TX_TIMER1_CCO_REG] ? RadioTxPacket[RADIO_TX_TIMER1_CCO_REG] : 200);
        }
        New_Bionode_CMD_Flag = 1;//Signal Main() to update RadioTxPacket[]
      }
      else{//TODO: Indicate ERROR, This should never happen
        __NOP();
      }

    }//end of if(!CRC_GEN...)
  }

  NVIC_EnableIRQ(UART0_IRQn);

}

void ADC_IRQHandler(void){
  uint8_t i;
  static _Bool isFirstSample = 1;
  static uint8_t ADC_Counter = 0;
  static uint16_t ADCResultOld = 0;
  static uint16_t ADCResultNew = 0;
  static int8_t DeltaFreq = 20; // Freqeuncy step is about 0.0195MHz
  //static uint16_t BaseFreqReg = 22784; //Initial Freq is 340MHz: 22784 = (340MHz/16Mhz - 16)*4096
 // static uint16_t BaseFreqReg = 0x518F;
 static uint16_t BaseFreqReg = 22784; //345MHz
  NVIC_DisableIRQ(ADC_IRQn);
  NRF_ADC->EVENTS_END = 0;
  Temp_Val = 0x3ff & NRF_ADC->RESULT;
  ADCResultNew =  0x3ff & NRF_ADC->RESULT;
  NRF_ADC->TASKS_STOP= 1; 
  if ((BaseFreqReg > MAX7060_MAXFREQ_VAL) || (BaseFreqReg < MAX7060_MINFREQ_VAL)){//Frequency out of limit
     //BaseFreqReg = 22784;//Reset to 340MHz
     BaseFreqReg = 22784; //345MHz
  }
  
  if (isFirstSample){
    ADCResultOld = ADCResultNew;
    isFirstSample = 0;
    BaseFreqReg = BaseFreqReg + DeltaFreq;
    MAX7060_Reg.FHigh0 = 0xff & (BaseFreqReg>>8);
    MAX7060_Reg.FHigh1 = 0xff & BaseFreqReg;
    MAX7060_Reg.FCenter0 = 0xff & (BaseFreqReg>>8);
    MAX7060_Reg.FCenter1 = 0xff & BaseFreqReg;
    MAX7060_Reg.FLow0 = 0xff & (BaseFreqReg>>8);
    MAX7060_Reg.FLow1 = 0xff & BaseFreqReg;
    MAX7060_Reg.FLoad_Reg = 1;
    MAX7060_Config(MAX7060_FHigh0_Reg_Addr, &MAX7060_Reg.FHigh0, (MAX7060_Fload_Reg_Addr - MAX7060_FHigh0_Reg_Addr + 1) );
  }
  else{
    if (ADCResultNew <= ADCResultOld) { //Better
      ADCResultOld = ADCResultNew;
      BaseFreqReg = BaseFreqReg + DeltaFreq;
      MAX7060_Reg.FHigh0 = 0xff & (BaseFreqReg >> 8);
      MAX7060_Reg.FHigh1 = 0xff & BaseFreqReg;
      MAX7060_Reg.FCenter0 = 0xff & (BaseFreqReg >> 8);
      MAX7060_Reg.FCenter1 = 0xff & BaseFreqReg;
      MAX7060_Reg.FLow0 = 0xff & (BaseFreqReg >> 8);
      MAX7060_Reg.FLow1 = 0xff & BaseFreqReg;
      MAX7060_Reg.FLoad_Reg = 1;
      MAX7060_Config(MAX7060_FHigh0_Reg_Addr, &MAX7060_Reg.FHigh0, (MAX7060_Fload_Reg_Addr - MAX7060_FHigh0_Reg_Addr + 1));

    }
    else{ //same or worse
      ADCResultOld = ADCResultNew;
      DeltaFreq = -DeltaFreq;
      BaseFreqReg = BaseFreqReg + DeltaFreq;
      MAX7060_Reg.FHigh0 = 0xff & (BaseFreqReg >> 8);
      MAX7060_Reg.FHigh1 = 0xff & BaseFreqReg;
      MAX7060_Reg.FCenter0 = 0xff & (BaseFreqReg >> 8);
      MAX7060_Reg.FCenter1 = 0xff & BaseFreqReg;
      MAX7060_Reg.FLow0 = 0xff & (BaseFreqReg >> 8);
      MAX7060_Reg.FLow1 = 0xff & BaseFreqReg;
      MAX7060_Reg.FLoad_Reg = 1;
      MAX7060_Config(MAX7060_FHigh0_Reg_Addr, &MAX7060_Reg.FHigh0, (MAX7060_Fload_Reg_Addr - MAX7060_FHigh0_Reg_Addr + 1));
    }

  }
  Temp_Freq = BaseFreqReg;
  NVIC_EnableIRQ(ADC_IRQn);
}

void RADIO_IRQHandler (void){
  static uint16_t PID_O, PID_N;
  static uint32_t TimeStamp;
  static uint32_t BionodeRestart_cnt=0;
  static uint32_t BionodeReTx_cnt=0;
  static _Bool sentFirstPkt = 0;
  uint16_t CRCVal;
  uint8_t i;
  NRF_RADIO->EVENTS_END = 0;
  NVIC_DisableIRQ(RADIO_IRQn);
  if (isRxEvent){
   // while (NRF_RADIO->EVENTS_DISABLED == 0){}// Wait for the radio to finish receiving 
   // NRF_RADIO->EVENTS_DISABLED = 0;
    if(NRF_RADIO->CRCSTATUS == 1){
      //NRF_TIMER2->TASKS_CLEAR = 1;
      LedGreen_Flag = 1;
      samplesWaited = 0;

      if (altTransmitListen) {
        altTransmitListen = 0;
        changePktLen = 0;
        NRF_GPIO->OUTSET = (1<<GREEN_LED_Pin)|(1<<BLUE_LED_Pin)|(1<<RED_LED_Pin);
        NRF_TIMER2->TASKS_STOP = 0x01;
        NRF_TIMER2->TASKS_CLEAR = 0x01;
        NRF_TIMER2->TASKS_START = 0X01;
        //NRF_RADIO->TASKS_RXEN = 1;//Enable Rx for next packet
        //NVIC_EnableIRQ(RADIO_IRQn);
        //return;
      }
      samplesWaited = 0;
      

      PID_N = ((SPIS_TX_FIFO.value[SPIS_TX_FIFO.end][2])<<8)|(SPIS_TX_FIFO.value[SPIS_TX_FIFO.end][3]);//Grab the new PID recv'd

      TimeStamp = NRF_TIMER0->CC[0];//Time stamp the packet
      SPIS_TX_FIFO.value[SPIS_TX_FIFO.end][SPIS_PACKET_LEN-6] = 0xff & (TimeStamp>>24);
      SPIS_TX_FIFO.value[SPIS_TX_FIFO.end][SPIS_PACKET_LEN-5] = 0xff & (TimeStamp>>16);
      SPIS_TX_FIFO.value[SPIS_TX_FIFO.end][SPIS_PACKET_LEN-4] = 0xff & (TimeStamp>>8);
      SPIS_TX_FIFO.value[SPIS_TX_FIFO.end][SPIS_PACKET_LEN-3] = 0xff & TimeStamp;

      SPIS_TX_FIFO.value[SPIS_TX_FIFO.end][1] = SPIS_TX_FIFO.cnt+1; //Insert buffer count into the packet

      //Generate a 2-byte CRC of the packet, excluding first byte (it's always 0x80)
      //CRCVal=CRC_Gen((uint8_t *) &(SPIS_TX_FIFO.value[SPIS_TX_FIFO.end][1]),55,CRCPOLYNOMIAL);
      //SPIS_TX_FIFO.value[SPIS_TX_FIFO.end][SPIS_PACKET_LEN-2] = 0xff & (CRCVal);
      //SPIS_TX_FIFO.value[SPIS_TX_FIFO.end][SPIS_PACKET_LEN-1] = 0xff & (CRCVal>>8);

      //Update buffer pointers and radio pointer
      cbDummyWrite_2D(&SPIS_TX_FIFO);
      NRF_RADIO->PACKETPTR = &(SPIS_TX_FIFO.value[SPIS_TX_FIFO.end][2]);

      NRF_GPIO->OUTSET = 1<<SPIS_DATARDY_Pin; //Pull P0.16 low to singal RPi new data is ready

      if((PID_N%Handshake_Interval) == 1){
        LedGreen_Flag=0;
        LedBlue_Flag=1;
      }
      if(lostConnection && PID_N>0x01) {
        lostConnection = 0;
      }
      if(((PID_N%Handshake_Interval) == 0) || lostConnection){
        if (PID_N ==0){
          BionodeRestart_cnt++;
          isRadioTxPacketReady = 1;
        }
        if (PID_N==PID_O){
          BionodeReTx_cnt++;
          isRadioTxPacketReady = 1;
        }
        if (lostConnection){
          BionodeReTx_cnt++;
          isRadioTxPacketReady = 1;
        }

        isRxEvent = 0;//Makes sure that when the the MCU knows the next radio interrupt comes from Tx end event
        if (isRadioTxPacketReady){
//          Handshake_Interval =(RadioTxPacket[POS_HANDSHAKE_INTERVAL_H]<<8) | RadioTxPacket[POS_HANDSHAKE_INTERVAL_L];
          while(NRF_RADIO->STATE != 0){ }
          NRF_RADIO->PACKETPTR = &RadioTxPacket;
          isRadioTxPacketReady = 0;

          if (changePktLen && (!sentFirstPkt)) {
            // If we've already sent the first packet, don't send another one from this conditional
            if (sentFirstPkt) {
              NRF_RADIO->PCNF1 = (NRF_RADIO->PCNF1 & ~(0xFFFF)) | 
                                 (RADIO_PACKET_LEN << RADIO_PCNF1_MAXLEN_Pos) | 
                                 (RADIO_PACKET_LEN << RADIO_PCNF1_STATLEN_Pos);
              isRxEvent = 1;
              NRF_RADIO->PACKETPTR = &SPIS_TX_FIFO.value[SPIS_TX_FIFO.end][2];
              NRF_RADIO->TASKS_RXEN = 1;//Enable Rx for next packet
              NVIC_EnableIRQ(RADIO_IRQn);
              return;
            }
            sentFirstPkt = 1;
          }
        }
        else{
          NRF_RADIO->PACKETPTR = &RadioTxPacket_Empty;
        }
        // RadioTxPacket[1]++;
        // Reduce the packet length to the length of a handshake
        NRF_RADIO->PCNF1 = (NRF_RADIO->PCNF1 & ~(0xFFFF)) | 
                           (HANDSHAKE_PACKET_LEN << RADIO_PCNF1_MAXLEN_Pos) |
                           (HANDSHAKE_PACKET_LEN << RADIO_PCNF1_STATLEN_Pos);
        isRxEvent = 0;
        NRF_RADIO->TASKS_TXEN = 1;
        // NRF_GPIO->OUTSET = 1<<T_R_CTRL_Pin;
        NVIC_EnableIRQ(RADIO_IRQn);
        return;
      }
      
      if (changePktLen && sentFirstPkt && (PID_N>0x01)) {
          if (RadioTxPacket[RADIO_TX_TIMER1_PRESCALE_REG] == 0x02) {
            RADIO_PACKET_LEN = LARGE_PACKET_LENGTH;
          } else {
            RADIO_PACKET_LEN = SMALL_PACKET_LENGTH;
          }
          SPIS_PACKET_LEN = RADIO_PACKET_LEN + 8;
          NRF_SPIS1->MAXTX = SPIS_PACKET_LEN;
          cbFlush_2D((CircularBuffer_2D *) &SPIS_TX_FIFO);

          NRF_RADIO->PCNF1 = (NRF_RADIO->PCNF1 & ~(0xFFFF)) | 
                             (HANDSHAKE_PACKET_LEN << RADIO_PCNF1_MAXLEN_Pos) | 
                             (HANDSHAKE_PACKET_LEN << RADIO_PCNF1_STATLEN_Pos);
          changePktLen = 0;
          sentFirstPkt = 0;
          altTransmitListen = 1;

          // Set up the timer to alternate at the settings of the bionode stim
          NRF_TIMER2->PRESCALER = prevPrescale; 
          NRF_TIMER2->CC[0] = prevCompare;
          NRF_TIMER2->TASKS_STOP = 0x01;
          NRF_TIMER2->TASKS_CLEAR = 0x01;

          NRF_RADIO->PACKETPTR = &RadioTxPacket;
          isRxEvent = 0;
          NRF_GPIO->OUTSET = (1<<GREEN_LED_Pin)|(1<<BLUE_LED_Pin);
          NRF_GPIO->OUT ^= 1<<RED_LED_Pin;
          while(NRF_RADIO->STATE !=0) { }
          NRF_RADIO->TASKS_TXEN = 1;
          NVIC_EnableIRQ(RADIO_IRQn);

          // Initialize the timer to alternate between transmiting and listening
          NRF_TIMER2->TASKS_START = 0x01;
          return;
      }

      if ((PID_N == PID_O+1)||((PID_N == 1) &&(PID_O == 0xFFFF))){
        PID_O = PID_N;
      }
      else{
        PacketLoss++;
        LedGreen_Flag=0;
        LedRed_Flag=1;
        PID_O = PID_N;
      }
    }
  }//end of if (isRxEvent)
  else{
      //Tx disabled should trigger this line of code always enter Rx after Tx
      if (altTransmitListen) {
        // Do nothing, the send/recieve structure is being handled by TIMER2
        NVIC_EnableIRQ(RADIO_IRQn);
        return;
      }
      // Extend the packet length back to the regular packet length
      NRF_RADIO->PCNF1 = (NRF_RADIO->PCNF1 & ~(0xFFFF)) | 
                         (RADIO_PACKET_LEN << RADIO_PCNF1_MAXLEN_Pos) | 
                         (RADIO_PACKET_LEN << RADIO_PCNF1_STATLEN_Pos);
      isRxEvent = 1;
      NRF_RADIO->PACKETPTR = &SPIS_TX_FIFO.value[SPIS_TX_FIFO.end][2];
  }
  if (!altTransmitListen) {
  NRF_RADIO->TASKS_RXEN = 1;//Enable Rx for next packet
  }
  NVIC_EnableIRQ(RADIO_IRQn);
  
}

void TIMER1_IRQHandler(void){ 
  static uint16_t One_Ms_Cnt = 0;
  static uint16_t ADC_Capture_Cnt =0;
  static uint8_t UART_Rx_Timeout_cnt = 0;
  NVIC_DisableIRQ(TIMER1_IRQn);
  NRF_TIMER1->EVENTS_COMPARE[0] = 0;
  if (ADC_Capture_Cnt == 49){
    ADC_Capture_Cnt = 0;
    if ((BS_Params_New.AutoPwrFreq_EN == 1) && (BS_Params_New.PA_EN==1)){
      NRF_ADC->TASKS_START = 1;
    }
  }
  if (One_Ms_Cnt>50){
    One_Ms_Cnt = 0;

    if (LedBlue_Flag==1){
      LedGreen_Flag=0;
      LedRed_Flag=0;
      NRF_GPIO->OUTSET = (1<<GREEN_LED_Pin)|(1<<RED_LED_Pin);
      NRF_GPIO->OUT ^= 1<<BLUE_LED_Pin;
      LedBlue_Flag=0;
    }
    else{
      NRF_GPIO->OUTSET = 1<<BLUE_LED_Pin;
    }

    if (LedRed_Flag==1){
      LedGreen_Flag=0;
      NRF_GPIO->OUTSET = (1<<GREEN_LED_Pin)|(1<<BLUE_LED_Pin);
      NRF_GPIO->OUT ^= 1<<RED_LED_Pin;
      LedRed_Flag=0;
    }
    else{
      NRF_GPIO->OUTSET = 1<<RED_LED_Pin;
    }

    if (LedGreen_Flag==1){
      NRF_GPIO->OUTSET = (1<<RED_LED_Pin)|(1<<BLUE_LED_Pin);
      NRF_GPIO->OUT ^= 1<<GREEN_LED_Pin;
      LedGreen_Flag=0;
    }
    else{
      NRF_GPIO->OUTSET = (1<<GREEN_LED_Pin);
    }

  }
  ADC_Capture_Cnt++;
  One_Ms_Cnt++;
  NVIC_EnableIRQ(TIMER1_IRQn);

}

void TIMER2_IRQHandler(void){
  static uint8_t isRecieveing = 0;
  static _Bool setRx = 0;
  static uint32_t wait = 0;
  NVIC_DisableIRQ(TIMER2_IRQn);
  NRF_TIMER2->EVENTS_COMPARE[0] = 0;

  if (altTransmitListen) {
    wait = (RADIO_PACKET_LEN - 10) * 2;
    if (samplesWaited > wait) {
      // Stop the radio from listening
      if (setRx) {
        NRF_RADIO->TASKS_DISABLE = 1;
        setRx = 0;
      }
      // Reduce the packet length to the length of a handshake
      NRF_RADIO->PCNF1 = (NRF_RADIO->PCNF1 & ~(0xFFFF)) | 
                         (HANDSHAKE_PACKET_LEN << RADIO_PCNF1_MAXLEN_Pos) |
                         (HANDSHAKE_PACKET_LEN << RADIO_PCNF1_STATLEN_Pos);
      isRxEvent = 0;
      uint8_t temp2 = (int)NRF_RADIO->STATE;
      while(NRF_RADIO->STATE != 0) {
        if (!altTransmitListen) {
          NVIC_EnableIRQ(TIMER2_IRQn);
          return;
        }
      }
      NRF_RADIO->PACKETPTR = &RadioTxPacket;
      NRF_RADIO->TASKS_TXEN = 1;
    } else if (!setRx) {
      // Wait for the radio to stop transmitting if necessary
      uint8_t temp = (int)NRF_RADIO->STATE;
      while(NRF_RADIO->STATE != 0) { 
        temp = (int)NRF_RADIO->STATE;
      }
      NRF_RADIO->PCNF1 = (NRF_RADIO->PCNF1 & ~(0xFFFF)) | 
                         (RADIO_PACKET_LEN << RADIO_PCNF1_MAXLEN_Pos) |
                         (RADIO_PACKET_LEN << RADIO_PCNF1_STATLEN_Pos);
      isRxEvent = 1;
      setRx = 1;
      NRF_RADIO->PACKETPTR = &SPIS_TX_FIFO.value[SPIS_TX_FIFO.end][2];
      NRF_RADIO->TASKS_RXEN = 1;
    }
    if (samplesWaited > (2 * wait)) {
      samplesWaited = 0;
    }
  } else {
    wait = (RADIO_PACKET_LEN - 10) * HANDSHAKE_PACKET_LEN * 10;
    if (samplesWaited > wait) {
      lostConnection = 1;
    }
    // TODO add condition to switch rx size for if BS looses power
    //if (lostConnection && (samplesWaited >= (wait*2)))
    //  NRF_RADIO->TASKS_DISABLE = 1;
    //  if (RADIO_PACKET_LEN == LARGE_PACKET_LENGTH) {
    //    RADIO_PACKET_LEN = SMALL_PACKET_LENGTH;
    //  } else if (RADIO_PACKET_LEN == SMALL_PACKET_LENGTH) {
    //    RADIO_PACKET_LEN = LARGE_PACKET_LENGTH;
    //  }
    //  samplesWaited = 0;
    //  isRxEvent = 1;
    //  uint8_t temp3 = (int)NRF_RADIO->STATE;
    //  while(NRF_RADIO->STATE != 0) { 
    //    temp3 = (int)NRF_RADIO->STATE;
    //    if (samplesWaited == 0) {
    //      NVIC_EnableIRQ(TIMER2_IRQn);
    //      return;
    //    }
    //  }
    //  NRF_RADIO->PCNF1 = (NRF_RADIO->PCNF1 & ~(0xFFFF)) | 
    //                     (RADIO_PACKET_LEN << RADIO_PCNF1_MAXLEN_Pos) |
    //                     (RADIO_PACKET_LEN << RADIO_PCNF1_STATLEN_Pos);
    //  isRxEvent = 1;
    //  NRF_RADIO->TASKS_RXEN = 1;
  }
  samplesWaited++;
  NVIC_EnableIRQ(TIMER2_IRQn);
}

//int Assemble_Packet(CircularBuffer_2D *cb){
//  uint16_t CRCVal;
//  Packet[0]=0xa5;
//  Packet[1]=0x5a;
//  cbRead_2D(cb,&Packet[2]);
//  CRCVal=CRC_Gen(&Packet[2], DEFAULT_DATA_LEN+6/*PID(2Bytes+Time Stamp(4Bytes)+Data(40Bytes)*/, CRCPOLYNOMIAL);
//  Packet[DEFAULT_UART_PACKET_LEN-2] = 0xff & (CRCVal);
//  Packet[DEFAULT_UART_PACKET_LEN-1] = 0xff & (CRCVal>>8);
//  CRCVal=CRC_Gen(&Packet[2], DEFAULT_DATA_LEN+8/*Data+ PID+ CRC*/, CRCPOLYNOMIAL);
//
//  return 0;
//}

/*
  CRC generator, LSbit first
*/
__inline uint16_t CRC_Gen (uint8_t *Data, uint8_t Len, uint16_t CRCPoly){
  uint16_t  Remainder = 0;
  uint8_t   i, j;
  for(i=0; i<Len; i++){
    Remainder ^= *(Data + i);
    for(j=0; j<8; j++){
      if (Remainder & 0x0001){
        Remainder = (Remainder>>1) ^ CRCPoly;
      }
      else{
        Remainder = Remainder>>1;
      }
    }
  }
  return Remainder;
}

__inline int RFGen_Init(uint32_t FreqReg, uint8_t OutputLevel){
  return 0;
}
__inline uint8_t SPI_TransferByte(uint8_t TxData){
  uint8_t RxData;
  NRF_SPI0->EVENTS_READY=0;
  NRF_SPI0->TXD=TxData;
  while (NRF_SPI0->EVENTS_READY==0){}
  RxData=NRF_SPI0->RXD;
  return RxData;
}

__inline void CSN_Low(void){
 NRF_GPIO->OUTCLR=1<<SPI0_CSN_Pin; 
 for(int i=0;i<100;i++){}
 return;
}

__inline void CSN_High(void){
 for(int i=0;i<100;i++){}
 NRF_GPIO->OUTSET=1<<SPI0_CSN_Pin; 
 return;
}

void SPI1_TWI1_IRQHandler (void){
  NVIC_DisableIRQ(SPI1_TWI1_IRQn);
  if (NRF_SPIS1->EVENTS_END==1){
    NRF_SPIS1->EVENTS_END = 0;
   // NRF_SPIS1->AMOUNTTX=0;
   // NRF_SPIS1->AMOUNTRX=0;

    //Update FIFO pointer and SPIS TX pointer
    cbDummyRead_2D(&SPIS_TX_FIFO);
    NRF_SPIS1->TXDPTR = &(SPIS_TX_FIFO.value[SPIS_TX_FIFO.start][0]);
    if (SPIS_TX_FIFO.cnt==0){
      NRF_GPIO->OUTCLR=1<<SPIS_DATARDY_Pin;
    }
  }
  NRF_SPIS1->TASKS_RELEASE=1;
  NVIC_EnableIRQ(SPI1_TWI1_IRQn);
}

uint8_t MAX7060_Config(uint8_t Initial_Reg_Addr, uint8_t *Data, uint8_t Len){
  int i;
  CSN_Low();
  SPI_TransferByte(0x01);
  SPI_TransferByte(Initial_Reg_Addr);
  for (i=0;i<Len;i++){
    SPI_TransferByte(*Data);
    Data++;
  }
  CSN_High();
  return 0;
}

uint8_t MAX7060_Reset(void){
  CSN_Low();
  SPI_TransferByte(0x04);
  CSN_High();
  return 0;
}