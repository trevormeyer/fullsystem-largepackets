/*
        demo-udp-03: udp-recv: a simple udp server
	receive udp messages

        usage:  udp-recv

        Paul Krzyzanowski
*/

#include <stdio.h>
#include <wiringPi.h>
#include <wiringSerial.h>

int UART_Init(int *fd){
  	char device[50] = "/dev/ttyS0";
  	const char *DevPtr = &device[0];

  	*fd=serialOpen(DevPtr, 115200);
  	if(*fd==-1){
    		printf("Cannot open serial port\n");
    		return -1;
 	}
 	return 0;
}
