/*
 * port.h
 *
 *  Created on: Oct 12, 2015
 *      Author: grant-linux
 */

#ifndef PORT_H_
#define PORT_H_

	#define TX_SERVICE_PORT	9000	/* hard-coded port number */
	#define RX_SERVICE_PORT	9001	/* hard-coded port number */

#endif /* PORT_H_ */
