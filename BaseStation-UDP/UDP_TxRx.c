#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>          
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include "port.h"
#include "UART.h"
#include "SPI.h"
#include <wiringPi.h>
#include <wiringSerial.h>
#include <wiringPiSPI.h>
#include <math.h>
//#define _BSD_SOURCE
#include<unistd.h>
#define BUFFSIZE 1024

#define SMALL_PACKET_LEN 58
#define LARGE_PACKET_LEN 198
#define BS_PACKET_LEN 58

#define CRCPOLYNOMIAL 0x8408

#define POS_RXID 5
#define POS_PACKET_TYPE 6
#define POS_ADC_TIMER1_PRESCALE_REG 23

#define ID_BS 1
#define ID_BIONODE 2
#define DATA_8BIT_LARGE 9
#define DATA_10BIT_LARGE 10
#define TYPE_SHUTDOWN 255

// Used for Data Transmission
int SPIS_PACKET_LEN = SMALL_PACKET_LEN;
// Used for BcastPkt
int DEFAULT_SPIS_PACKET_LEN = SMALL_PACKET_LEN;
int DEFAULT_UDP_PACKET_LEN = BS_PACKET_LEN;

unsigned char SPIData[LARGE_PACKET_LEN]={0xa5,0x5a, 0x00, 0x00, 0x02,0x00,0x00};
// FIXME Change length to a variable
unsigned char BcastPKT[SMALL_PACKET_LEN]={0xa5,0x5a, 0x00, 0x00, 0x02,0x00,0x06};

uint16_t CRC_Gen (uint8_t *Data, uint8_t Len, uint16_t CRCPoly);

struct sockaddr_in myaddr, remaddr, remaddr_bcast;
int fd_sock, fd_uart, slen = sizeof(remaddr);
int recvlen; /* # bytes in acknowledgement message */
char *bcast_addr = "192.168.42.255";
int isPairingActive = 1;
struct sockaddr_in *remaddr_ptr = NULL;
uint8_t change_packet_len = 0;

int chan=0;
int speed=1000000;


void *UDP_TX_Bcast_Runner(void *argument) {
	printf("UDP Tx thread created\n");
	uint16_t CRCVal = 0;
	while(1) {
			// printf("Sending Broadcast\n");                                    REMEMBER TO UNCOMMENT ME
			CRCVal=CRC_Gen(&BcastPKT[2], (sizeof(BcastPKT)-4) , CRCPOLYNOMIAL);
		    BcastPKT[DEFAULT_SPIS_PACKET_LEN-2] = 0xff & CRCVal;
		    BcastPKT[DEFAULT_SPIS_PACKET_LEN-1] = 0xff & (CRCVal>>8);
			if (sendto(fd_sock, &BcastPKT, sizeof(BcastPKT), 0,
					(struct sockaddr *) &remaddr_bcast, slen) == -1) {
				perror("sendto: ");
				exit(EXIT_FAILURE);
			}
			sleep(2);	
	}

}


void *UDP_TX_Runner(void *argument) {
	//int passed_in_value;
	//passed_in_value = *((int *) argument);
    uint16_t CRCVal=0;
	printf("UDP Tx thread created\n");

	/*Setup UDP Tx Socket*/
//	if ((fd = socket(AF_INET, SOCK_DGRAM, 0)) == -1) {
//		perror("Socket creating failed: ");
//		exit(EXIT_FAILURE);
//	}
//	memset((char *) &myaddr_tx, 0, sizeof(myaddr_tx));
//	myaddr_tx.sin_family = AF_INET;
//	myaddr_tx.sin_addr.s_addr = htonl(INADDR_ANY);
//	myaddr_tx.sin_port = htons(0);
//
//	int UDP_BcastEnable = 1;
//	if ((setsockopt(fd, SOL_SOCKET, SO_BROADCAST, &UDP_BcastEnable,
//			sizeof(UDP_BcastEnable))) == -1) {
//		perror("Cannot set socket option (enable UDP broadcast): ");
//	}
//
//	if (bind(fd, (struct sockaddr *) &myaddr_tx, sizeof(myaddr_tx)) == -1) {
//		perror("Bind failed: ");
//		exit(EXIT_FAILURE);
//	}
//
//	memset((char *) &remaddr_tx, 0, sizeof(remaddr_tx));
//	remaddr_tx.sin_family = AF_INET;
//	remaddr_tx.sin_port = htons(TX_SERVICE_PORT);
//	if (inet_pton(AF_INET, client_addr, &remaddr_tx.sin_addr) == 0) {
//		printf("inet_aton() failed\n");
//		exit(EXIT_FAILURE);
//	}
	int j = 1;
	while (1) {
		if (remaddr_ptr == NULL) {
			if (isPairingActive) {
				printf("Sending Broadcast\n");
				CRCVal=CRC_Gen(&BcastPKT[2], (sizeof(BcastPKT)-4) , CRCPOLYNOMIAL);
			    BcastPKT[SPIS_PACKET_LEN-2] = 0xff & CRCVal;
			    BcastPKT[SPIS_PACKET_LEN-1] = 0xff & (CRCVal>>8);
				if (sendto(fd_sock, &BcastPKT, sizeof(BcastPKT), 0,
						(struct sockaddr *) &remaddr_bcast, slen) == -1) {
					perror("sendto: ");
					//exit(EXIT_FAILURE);
				}
				sleep(1);
			}

		} else {
			  while(1){
			    if (digitalRead(5) == 1){
			      digitalWrite (10,  LOW);
			      wiringPiSPIDataRW(chan, &SPIData[0], SPIS_PACKET_LEN);
			      //wiringPiSPIDataRW(chan, &SPIData[0], (POS_PACKET_TYPE+1));
			      //wiringPiSPIDataRW(chan, &SPIData[POS_PACKET_TYPE+1], (SPIS_PACKET_LEN-POS_PACKET_TYPE-1));
			      digitalWrite (10, HIGH);

      			if ((SPIData[POS_PACKET_TYPE]==DATA_8BIT_LARGE) || (SPIData[POS_PACKET_TYPE]==DATA_10BIT_LARGE)) {
      				// We should have a large packet length for this packet type
      				// If this is not true, then we know the change has occured
      				if (SPIS_PACKET_LEN != LARGE_PACKET_LEN) {
 									SPIS_PACKET_LEN = LARGE_PACKET_LEN;
 									change_packet_len = 0;
 									continue;
 								}
						} else {
      				// We should have a small packet length for this packet type
      				// If this is not true, then we know the change has occured
      				if (SPIS_PACKET_LEN != SMALL_PACKET_LEN) {
								SPIS_PACKET_LEN = SMALL_PACKET_LEN;
								change_packet_len = 0;
								continue;
							}
						} 

						//printf("Packets from MCU!!\r\n");

			     	/*
			      for(int i=0;i<DEFAULT_SPIS_PACKET_LEN;i++){
			       	printf("%02x ",SPIData[i]);
			     	}
			      printf("\r\n");
			      */
			    
			      SPIData[0]=0xa5;
			      SPIData[1]=0x5a;
			     	//SPIData[4]=0x03;
			     	//SPIData[5]=0x00;
			     	//SPIData[6]=0x00;
			                
			      CRCVal=CRC_Gen(&SPIData[2], (SPIS_PACKET_LEN-4) , CRCPOLYNOMIAL);
			      //printf("CRCVal is %04x\n", CRCVal);
			      /*Lease significant byte first*/
			      SPIData[SPIS_PACKET_LEN-2] = 0xff & CRCVal;
			      SPIData[SPIS_PACKET_LEN-1] = 0xff & (CRCVal>>8);
			     
			      if (SPIData[6] == 3){
			      	for(int i=0;i<SPIS_PACKET_LEN;i++){
			       		printf("%02x ",SPIData[i]);
			       	}
			      	printf("\r\n");
						}
			      //printf("%04x\n",CRC_Gen(&SPIData[2], sizeof(DEFAULT_SPIS_PACKET_LEN)-2, CRCPOLYNOMIAL));
			      
			      //	remaddr_ptr->sin_port = htons(TX_SERVICE_PORT);
			     	if (sendto(fd_sock, &SPIData, SPIS_PACKET_LEN, 0, (struct sockaddr *)&remaddr, slen)==-1) {
			        	perror("sendto");
				       	//exit(EXIT_FAILURE);
			     	}
			      	// j++;
			      	// if (j==10){
			       //  	//return 0;
			      	// }
			    }

			 }
			//int static k=0;
			// for (int i=0;i<40; i++){
			// 	SPIData[i+7] =  (uint8_t) (sinf((float)k/1*2*3.14159)*100 + 128);
			// 	k+=100;
			// }


			// CRCVal=CRC_Gen(&SPIData[2], (sizeof(SPIData)-4) , CRCPOLYNOMIAL);
		 //    SPIData[DEFAULT_SPIS_PACKET_LEN-2] = 0xff & CRCVal;
		 //    SPIData[DEFAULT_SPIS_PACKET_LEN-1] = 0xff & (CRCVal>>8);
		    // for(int i=0;i<DEFAULT_SPIS_PACKET_LEN;i++){
		    // 	printf("%02x ",SPIData[i]);
		    // }
		    //printf("\r\n");
		    
			//printf("MSG No.%d is sent to PC\n", j++);
			//usleep(8000);
		}

	}

	return NULL;
}

void *UDP_RX_Runner(void *argument) {
	//int passed_in_value;
	//passed_in_value = *((int *) argument);
	char rx_buf[BUFFSIZE];
	char client_ip[INET_ADDRSTRLEN];

	socklen_t addrlen = sizeof(remaddr);
	printf("UDP Rx thread created\n");
	/*Setup UDP rx Socket*/ //	if ((fd = socket(AF_INET, SOCK_DGRAM, 0)) == -1) {
//		perror("Socket creating failed: ");
//		exit(EXIT_FAILURE);
//	}
//	memset((char *) &myaddr_rx, 0, sizeof(myaddr_rx));
//	myaddr_rx.sin_family = AF_INET;
//	myaddr_rx.sin_addr.s_addr = htonl(INADDR_ANY);
//	myaddr_rx.sin_port = htons(RX_SERVICE_PORT);
//
//	if (bind(fd, (struct sockaddr *) &myaddr_rx, sizeof(myaddr_rx)) == -1) {
//		perror("Bind failed: ");
//		exit(EXIT_FAILURE);
//	}
	//memset((char *) &remaddr_rx, 0, sizeof(remaddr_rx));
	// remaddr_rx.sin_family = AF_INET;
	//remaddr_rx.sin_port = htons(TX_SERVICE_PORT);
	//if (inet_pton(AF_INET, client_addr, &remaddr_rx.sin_addr)==0) {
	//  printf("inet_aton() failed\n");
	// exit(EXIT_FAILURE);
	// }
	printf("waiting on port %d\n", RX_SERVICE_PORT);
	while (1) {
    recvlen = recvfrom(fd_sock, rx_buf, BUFFSIZE, 0, (struct sockaddr *) &remaddr, &addrlen);
		if(recvlen == DEFAULT_UDP_PACKET_LEN ){
			//if (1){
			//	recvlen = recvfrom(fd, rx_buf, BUFFSIZE, 0,
			//		(struct sockaddr *) &remaddr, &addrlen);
			inet_ntop(AF_INET, &(remaddr.sin_addr), &client_ip[0], INET_ADDRSTRLEN);
			printf("\nGot Data from %s, of length %d\n\n", &client_ip[0],recvlen);

			if (remaddr_ptr == NULL) {
				if (isPairingActive) {
					//remaddr.sin_port = htons(TX_SERVICE_PORT);
					remaddr_ptr = &remaddr;
					printf("Paired\n");
					isPairingActive = 0;
					printf("Sending Packet to UART\n");
					for(int i=0;i<DEFAULT_UDP_PACKET_LEN;i++){
					 	serialPutchar(fd_uart, rx_buf[i]);
					}
				}
			} else {
				if((rx_buf[POS_RXID]==ID_BS) && (rx_buf[POS_PACKET_TYPE]==TYPE_SHUTDOWN)){
		    		//Recv'd shutdown cmd; shutting down RPi
                    system("sudo shutdown now -h");
                    exit(EXIT_SUCCESS);
        }
			  //TODO: send packet to UART
				printf("Sending Packet to UART\n");
				for(int i=0;i<DEFAULT_UDP_PACKET_LEN;i++){
                     serialPutchar(fd_uart, rx_buf[i]);
            	}

			}
		}
		//remaddr.sin_port = htons(TX_SERVICE_PORT);
	}
	return NULL;
}
int main(int argc, char*argv[]) {
	pthread_t tid_udp_tx;
	pthread_t tid_udp_rx;
	pthread_t tid_udp_tx_bcast;
	int UDP_BcastEnable = 1;
	int thread_args_udp_tx = 10;
	int thread_args_udp_rx = 100;
	int thread_args_udp_tx_bcast = 1000;
	int ret;
	wiringPiSetup();
	UART_Init(&fd_uart);
	SPI_Init(chan,speed);
	printf("ptr value is %lu\n",(long unsigned int)remaddr_ptr);
	/*Setup UDP Tx Rx Sockets*/
	if ((fd_sock = socket(AF_INET, SOCK_DGRAM, 0)) == -1) {
		perror("Socket creating failed: ");
		exit(EXIT_FAILURE);
	}
	memset((char *) &myaddr, 0, sizeof(myaddr));
	myaddr.sin_family = AF_INET;
	myaddr.sin_addr.s_addr = htonl(INADDR_ANY);
	myaddr.sin_port = htons(RX_SERVICE_PORT);

	memset((char *) &remaddr, 0, sizeof(remaddr));
	remaddr.sin_family = AF_INET;
	remaddr.sin_port = htons(TX_SERVICE_PORT);

	memset((char *) &remaddr, 0, sizeof(remaddr_bcast));
	remaddr_bcast.sin_family = AF_INET;
	remaddr_bcast.sin_port = htons(TX_SERVICE_PORT);
	if (inet_pton(AF_INET, bcast_addr, &remaddr_bcast.sin_addr) == 0) {
		printf("inet_pton() failed\n");
		exit(EXIT_FAILURE);
	}

	if ((setsockopt(fd_sock, SOL_SOCKET, SO_BROADCAST, &UDP_BcastEnable,
			sizeof(UDP_BcastEnable))) == -1) {
		perror("Cannot set socket option (enable UDP broadcast): ");
	}

	if (bind(fd_sock, (struct sockaddr *) &myaddr, sizeof(myaddr)) == -1) {
		perror("Bind failed: ");
		exit(EXIT_FAILURE);
	}

	//Create UDP Tx Thread
	printf("In main: creating UDP Tx thread\n");
	if ((ret = pthread_create(&tid_udp_tx, NULL, UDP_TX_Runner,
			(void *) &thread_args_udp_tx))) {
		perror("pthread_create: ");
		exit(EXIT_FAILURE);
	}

	//Create UDP Rx Thread
	printf("In main: creating UDP Rx thread\n");
	if ((ret = pthread_create(&tid_udp_rx, NULL, UDP_RX_Runner,
			(void *) &thread_args_udp_rx))) {
		perror("pthread_create: ");
		exit(EXIT_FAILURE);
	}

	if ((ret = pthread_create(&tid_udp_tx_bcast, NULL, UDP_TX_Bcast_Runner,
			(void *) &thread_args_udp_tx_bcast))) {
		perror("pthread_create: ");
		exit(EXIT_FAILURE);
	}

	if ((ret = pthread_join(tid_udp_tx, NULL))) {
		perror("pthread_join(UDP Tx Thread): ");
		exit(-1);
	}

	if ((ret = pthread_join(tid_udp_rx, NULL))) {
		perror("pthread_join(UDP Rx Therad): ");
		exit(EXIT_FAILURE);
	}

	if ((ret = pthread_join(tid_udp_tx_bcast, NULL))) {
		perror("pthread_join(UDP Rx Therad): ");
		exit(EXIT_FAILURE);
	}

	printf("In main: All threads completed successfully\n");
	exit(EXIT_SUCCESS);
}

uint16_t CRC_Gen (uint8_t *Data, uint8_t Len, uint16_t CRCPoly){
  uint16_t  Remainder = 0;
  uint8_t   i, j;
  for(i=0; i<Len; i++){
    Remainder ^= *(Data + i);
    for(j=0; j<8; j++){
      if (Remainder & 0x0001){
        Remainder = (Remainder>>1) ^ CRCPoly;
      }
      else{
        Remainder = Remainder>>1;
      }
    }

  }

  return Remainder;
}


