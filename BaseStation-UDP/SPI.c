#include <wiringPi.h>
#include <wiringPiSPI.h>
#include <stdio.h>
#include "SPI.h"

int SPI_Init(int chan, int speed){
  pinMode (5, INPUT) ;
  if( wiringPiSPISetup(chan, speed)==-1)
  {
    printf("Could not initialise SPI\n");
    return -1;
  }
  return 0;

}

// #define BUFLEN 2048

// void printsth(void);
// //PID(2 Bytes) + TxID(1 Byte) + RxID(1 Byte) + Type(1 Byte) + Data(40 Bytes) + StimStat(5 Bytes) + Time Stamp(4 Bytes)
// #define DEFAULT_PAYLOAD_LEN 54
// #define DEFAULT_SPIS_PACKET_LEN DEFAULT_PAYLOAD_LEN+4 //Packet valid indicator byte, buffer cnt byte, 2-byte CRC

// #define CRCPOLYNOMIAL 0x8408
// uint16_t CRC_Gen (uint8_t *Data, uint8_t Len, uint16_t CRCPoly);

// int main (void)
// {
//   int i,j=0;
//   int chan=0;
//   int speed=1000000;
//   void (*functionPtr)(void);
//   functionPtr=&printsth;

// /*Setup UDP*/
//   struct sockaddr_in myaddr, remaddr;
//   int fd, slen=sizeof(remaddr);
//   int recvlen;		/* # bytes in acknowledgement message */
//   char *server = "192.168.1.4";	/* change this to use a different server */

//   /* create a socket */
//   if ((fd=socket(AF_INET, SOCK_DGRAM, 0))==-1)
//     printf("socket created\n");

//   /* bind it to all local addresses and pick any port number */
	
//   memset((char *)&myaddr, 0, sizeof(myaddr));
//   myaddr.sin_family = AF_INET;
//   myaddr.sin_addr.s_addr = htonl(INADDR_ANY);
//   myaddr.sin_port = htons(0);

//   if (bind(fd, (struct sockaddr *)&myaddr, sizeof(myaddr)) < 0) {
//     perror("bind failed");
//     return 0;
//   }       

//   /* now define remaddr, the address to whom we want to send messages */
//   /* For convenience, the host address is expressed as a numeric IP address */
//   /* that we will convert to a binary format via inet_aton */

//   memset((char *) &remaddr, 0, sizeof(remaddr));
//   remaddr.sin_family = AF_INET;
//   remaddr.sin_port = htons(TX_SERVICE_PORT);
//   if (inet_aton(server, &remaddr.sin_addr)==0) {
//     fprintf(stderr, "inet_aton() failed\n");
//     exit(1);
//   }
// /*End of UDP setup*/

//   wiringPiSetup () ;
//   pinMode (5, INPUT) ;
//   //pinMode(10, OUTPUT);
//   //pinMode(10, HIGH);

//   //wiringPiISR (5, INT_EDGE_BOTH, functionPtr);
//   unsigned char SPIData[DEFAULT_SPIS_PACKET_LEN]={0xa5,0x5a};
//   if( wiringPiSPISetup(chan, speed)==-1)
//   {
//     printf("Could not initialise SPI\n");
//     return 0;
//   }

//   while(1){
//     uint16_t CRCVal=0;
//     if (digitalRead(5) == 0){
//       digitalWrite (10,  LOW) ;
//       wiringPiSPIDataRW(chan, &SPIData[0], DEFAULT_SPIS_PACKET_LEN);
//       digitalWrite (10, HIGH) ; 

//       /*
//       for(i=0;i<DEFAULT_SPIS_PACKET_LEN;i++){
//         printf("%02x ",SPIData[i]);
//       }
//       printf("\r\n");
//       */
    
//       SPIData[0]=0xa5;
//       SPIData[1]=0x5a;
//      //SPIData[4]=0x03;
//      //SPIData[5]=0x00;
//      //SPIData[6]=0x00;
                
//       CRCVal=CRC_Gen(&SPIData[2], (sizeof(SPIData)-4) , CRCPOLYNOMIAL);
//       //printf("CRCVal is %04x\n", CRCVal);
//       /*Lease significant byte first*/
//       SPIData[DEFAULT_SPIS_PACKET_LEN-2] = 0xff & CRCVal;
//       SPIData[DEFAULT_SPIS_PACKET_LEN-1] = 0xff & (CRCVal>>8);
     
       
//       for(i=0;i<DEFAULT_SPIS_PACKET_LEN;i++){
//         printf("%02x ",SPIData[i]);
//       }
//       printf("\r\n");
//       printf("%04x\n",CRC_Gen(&SPIData[2], sizeof(DEFAULT_SPIS_PACKET_LEN)-2, CRCPOLYNOMIAL));
      

//      if (sendto(fd, &SPIData, sizeof(SPIData), 0, (struct sockaddr *)&remaddr, slen)==-1) {
//         perror("sendto");
// 	       exit(1);
//      }
//       j++;
//       if (j==10){
//         //return 0;
//       }
//     }

//   }
//   while(1);
//   //digitalWrite (10,  LOW) ;
//   //wiringPiSPIDataRW(chan, &SPIData[0], DEFAULT_SPIS_PACKET_LEN);

//   //digitalWrite (10, HIGH) ; 
//   delayMicroseconds (200);
//   pinMode(10, HIGH);
//   //printf("%s\n",SPIData);
//   //printf("Time!!%d\n",waitForInterrupt(0,1000));
//   while(1){}
//   return 0 ;
// }

//  void printsth(void){
//   printf("helloworld\n");
//   return;
// }

// uint16_t CRC_Gen (uint8_t *Data, uint8_t Len, uint16_t CRCPoly){
//   uint16_t  Remainder = 0;
//   uint8_t   i, j;
//   for(i=0; i<Len; i++){
//     Remainder ^= *(Data + i);
//     for(j=0; j<8; j++){
//       if (Remainder & 0x0001){
//         Remainder = (Remainder>>1) ^ CRCPoly;
//       }
//       else{
//         Remainder = Remainder>>1;
//       }
//     }

//   }

//   return Remainder;
// }



